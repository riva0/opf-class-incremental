# this script requires four arguments:
# $1: the dataset name (i.e. if running tests for 'dataset1.dat', the dataset name is 'dataset')
# $2: the number of classes in said dataset
# $3: cost of SVM
# $4: gamma of SVM
echo "Simulating experiment for $1, with $2 classes."

incsize=$5
initsize=$((100/incsize))


# Runs experiments where all classes begin as S_0
for (( i=1; i<=$2; i++ ))
do
	cp -f $1.opf $1.dat
	# Splits the dataset into S and T
	echo "	Splitting 'S' and 'T'..."
	{
		echo "./opf_split $1.dat 0.5 0 0.5 1"
		./opf_split $1.dat 0.5 0 0.5 1
		mv -f training.dat $1S.dat
		mv -f testing.dat $1T.dat
	} &>> /dev/null

	# Converts T to svm
	echo "	Converting T to SVM..."
	{
		./../tools/opf2txt $1T.dat $1T.txt
		./../tools/txt2svm $1T.txt $1T.svm
		rm -f $1T.txt
	} &>> /dev/null

	echo "	S_0 is of class $i"

	# Splits the S dataset into one file for each class
	echo "	Splitting classes"
	{
		echo "./opf_class_split $1S.dat 0"
		./opf_class_split $1S.dat 0
	} &>> /dev/null
	# Splits the initial class's file into two subsets; one is S_0, the other belongs to I
	echo "	Splitting initial class"
	{
		echo "./opf_split class$i.dat 0.5 0 0.5 0"
		./opf_split class$i.dat 0.5 0 0.5 0
	} &>> /dev/null
	mv -f training.dat $1S_0.dat
	mv -f testing.dat class$i.dat

	# build 1-class model on S_0 with SVM and test S_0 against T
	echo "	Initial training on S_0"
	{
		cp -f $1T.svm testS_0.svm
		./../tools/opf2txt $1S_0.dat $1S_0.txt
		./../tools/txt2svm $1S_0.txt $1S_0.svm
		rm -f $1S_0.txt
		echo "./svm-train -s 2 $1S_0.svm"
		./svm-train -s 2 $1S_0.svm
		mv svmtime.time trainS_0.svm.time
		echo "./svm-predict testS_0.svm $1S_0.svm.model out.svm"
		./svm-predict testS_0.svm $1S_0.svm.model out.svm
		mv svmtime.time testS_0.svm.time
		#mv svmacc.acc testS_0.svm.acc
		cp -f $1T.dat testS_0.dat
		mv out.svm testS_0.dat.out
		./opf_accuracy testS_0.dat
		mv testS_0.dat.acc testS_0.svm.acc
	} &>> /dev/null 


	cp $1S_0.dat merged.dat
	# Running experiment with class-unique increments
	echo "Incremental-splitting each class file..."
	for (( j=1; j<=$2; j++ ))
	do
		{
			mv -f class$j.dat $1$(printf "%d" $j)I.dat
			echo "./opf_incremental_split $1$(printf "%d" $j)I.dat 1 0 0.$(printf "%02d" $initsize) $incsize 0"
			./opf_incremental_split $1$(printf "%d" $j)I.dat 1 0 0.$(printf "%02d" $initsize) $incsize 0
		} &>> /dev/null

		# Iterates through all segments
		for (( k=0; k<$incsize; k++ ))
		do
			echo "	Currently inc_training on $(printf "%d" $j)II_$(printf "%04d" $k)"
			{
				echo "./opf_merge merged.dat $1$(printf "%d" $j)II_$(printf "%04d" $k).dat"
				./opf_merge merged.dat $1$(printf "%d" $j)II_$(printf "%04d" $k).dat
				cp merged.dat $1$(printf "%d" $j)II_$(printf "%04d" $k).dat

				cp -f $1T.svm test$(printf "%d" $j)II_$(printf "%04d" $k).svm
				./../tools/opf2txt $1$(printf "%d" $j)II_$(printf "%04d" $k).dat $1$(printf "%d" $j)II_$(printf "%04d" $k).txt
				./../tools/txt2svm $1$(printf "%d" $j)II_$(printf "%04d" $k).txt $1$(printf "%d" $j)II_$(printf "%04d" $k).svm
				rm -f $1$(printf "%d" $j)II_$(printf "%04d" $k).txt
				echo "./svm-train -c $3 -g $4 $1$(printf "%d" $j)II_$(printf "%04d" $k).svm"
				./svm-train -c $3 -g $4 $1$(printf "%d" $j)II_$(printf "%04d" $k).svm
				mv svmtime.time train$(printf "%d" $j)II_$(printf "%04d" $k).svm.time
				echo "./svm-predict test$(printf "%d" $j)II_$(printf "%04d" $k).svm $1$(printf "%d" $j)II_$(printf "%04d" $k).svm.model out.svm"
				./svm-predict test$(printf "%d" $j)II_$(printf "%04d" $k).svm $1$(printf "%d" $j)II_$(printf "%04d" $k).svm.model out.svm
				mv svmtime.time test$(printf "%d" $j)II_$(printf "%04d" $k).svm.time
				#mv svmacc.acc test$(printf "%d" $j)II_$(printf "%04d" $k).svm.acc
				cp -f $1T.dat test$(printf "%d" $j)II_$(printf "%04d" $k).dat
				mv out.svm test$(printf "%d" $j)II_$(printf "%04d" $k).dat.out
				./opf_accuracy test$(printf "%d" $j)II_$(printf "%04d" $k).dat
				mv test$(printf "%d" $j)II_$(printf "%04d" $k).dat.acc test$(printf "%d" $j)II_$(printf "%04d" $k).svm.acc

			} &>> /dev/null
		done
	done

	# finally, testing full training on 'S' against 'T'
	echo "	Full training..."
	{
		cp -f $1T.svm testFull.svm
		./../tools/opf2txt $1S.dat $1S.txt
		./../tools/txt2svm $1S.txt $1S.svm
		rm -f $1S.txt
		echo "./svm-train -c $3 -g $4 $1S.svm"
		./svm-train -c $3 -g $4 $1S.svm
		mv svmtime.time trainFull.svm.time
		echo "./svm-predict testFull.svm $1S.svm.model out.svm"
		./svm-predict testFull.svm $1S.svm.model out.svm
		mv svmtime.time testFull.svm.time
		#mv svmacc.acc testFull.svm.acc
		cp -f $1T.dat testFull.dat
		mv out.svm testFull.dat.out
		./opf_accuracy testFull.dat
		mv testFull.dat.acc testFull.svm.acc

	} &>> /dev/null

	echo "Outputting acc to acc file..."
	{
		echo "Full Accuracy"
		cat testFull.svm.acc

		echo "Accuracy - I"
		cat testS_0.svm.acc
		for (( j=1; j<=$2; j++ ))
		do
			for (( k=0; k<$incsize; k++ ))
			do
				cat test$(printf "%d" $j)II_$(printf "%04d" $k).svm.acc
			done
		done


	} &> resultsB/$1/svm/accK$i.txt


	echo "Outputting time to time file..."
	{
		echo "Full Time (seconds)"
		cat testFull.svm.time

		echo "Incremental Time - I"
		cat trainS_0.svm.time
		for (( j=1; j<=$2; j++ ))
		do
			for (( k=0; k<$incsize; k++ ))
			do
				cat train$(printf "%d" $j)II_$(printf "%04d" $k).svm.time
			done
		done

		echo "Classification Time - C"
		cat testS_0.svm.time
		for (( j=1; j<=$2; j++ ))
		do
			for (( k=0; k<$incsize; k++ ))
			do
				cat test$(printf "%d" $j)II_$(printf "%04d" $k).svm.time
			done
		done
	} &> resultsB/$1/svm/timeK$i.txt

	rm -f *.svm* *.dat*
done