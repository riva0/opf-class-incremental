#include "OPFactive.h"

// Split subgraph into multiple parts, one for each class.
Subgraph** opf_SplitSubgraphClasses(Subgraph *sg){
	int i;
	int j;

	// Allocating vector of subclasses
	Subgraph** classSg = (Subgraph**) malloc(sg->nlabels*sizeof(Subgraph*));

	// Counting size of classes
	int* classSizes = (int*)calloc(sg->nlabels,sizeof(int));
	for (i = 0; i < sg->nnodes; i++)
		classSizes[sg->node[i].truelabel-1] += 1;

	// Creating individual class subgraphs
	for (i = 0; i < sg->nlabels; i++){
		classSg[i] = CreateSubgraph(classSizes[i]);
		(classSg[i])->nlabels = sg->nlabels;
		(classSg[i])->nfeats = sg->nfeats;
		for (j=0; j < (classSg[i])->nnodes; j++)
			(classSg[i])->node[j].feat = AllocFloatArray((classSg[i])->nfeats);
	}
	
	for (i = 0; i < sg->nlabels; i++)
		classSizes[i] = 0;
	// Completing individual class subgraphs
	for (i = 0; i < sg->nnodes; i++){
		int classIndex = sg->node[i].truelabel-1;
		(classSg[classIndex])->node[classSizes[classIndex]].position = sg->node[i].position;
		for (j = 0; j < sg->nfeats; j++)
			(classSg[classIndex])->node[classSizes[classIndex]].feat[j] = sg->node[i].feat[j];
		(classSg[classIndex])->node[classSizes[classIndex]].truelabel = sg->node[i].truelabel;

		classSizes[classIndex] += 1;
	}


	free(classSizes);
	return classSg;
}