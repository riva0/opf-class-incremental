#!/bin/bash
datasets=(cone-torus SpamBase2 mpeg7_BAS ntl skin Produce L3_4dist)
labels=(3 2 70 2 2 14 3)
kmax=(40 50 20 50 100 50 50)
c=(1024 32 32 32 2048 32 0.125)
g=(1 0.0078125 0.0078125 8 0.5 0.0078125 2)
splits=(20 20 5 20 20 20 20)

rm -rf resultsA
mkdir resultsA

for i in 0 1 3 5 6
do
	echo "Running test for set ${datasets[$i]}"

	for (( j=1; j<=10; j++ ))
	do
		mkdir resultsA/${datasets[$i]}
		mkdir resultsA/${datasets[$i]}/opf
		mkdir resultsA/${datasets[$i]}/svm
		mkdir resultsA/${datasets[$i]}/knn1
		mkdir resultsA/${datasets[$i]}/knn3
		mkdir resultsA/${datasets[$i]}/knn5
		echo "Iteration $j"
		cp -f data/${datasets[$i]}.opf ${datasets[$i]}.opf
		./run_OPFA_on_dataset.sh ${datasets[$i]} ${labels[$i]} ${kmax[$i]} ${splits[$i]}
		./run_knnA_on_dataset.sh ${datasets[$i]} ${labels[$i]} 1 ${splits[$i]}
		./run_knnA_on_dataset.sh ${datasets[$i]} ${labels[$i]} 3 ${splits[$i]}
		./run_knnA_on_dataset.sh ${datasets[$i]} ${labels[$i]} 5 ${splits[$i]}
		./run_SVMA_on_dataset.sh ${datasets[$i]} ${labels[$i]} ${c[$i]} ${g[$i]} ${splits[$i]}
		mv -f resultsA/${datasets[$i]} resultsA/${datasets[$i]}$j
	done

done
