#!/bin/bash
datasets=(cone-torus SpamBase2 mpeg7_BAS ntl skin)
labels=(3 2 7 2 2)
c=(2048 512 2 128 2048)
g=(0.5 0.000122 0.0078125 8 0.5)


mkdir resultsB

for i in 0 1 3 4
do
	echo "Running test for set ${datasets[$i]}" >> log.txt

	for (( j=1; j<=10; j++ ))
	do
		mkdir resultsB/${datasets[$i]}
		mkdir resultsB/${datasets[$i]}/svm
		echo "Iteration $j" >> log.txt
		cp -f data/${datasets[$i]}.opf ${datasets[$i]}.opf
		./run_SVMB_on_dataset.sh ${datasets[$i]} ${labels[$i]} ${c[$i]} ${g[$i]}
		mv -f resultsB/${datasets[$i]} resultsB/${datasets[$i]}$j
	done

done
