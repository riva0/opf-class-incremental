#!/bin/bash
datasets=(cone-torus SpamBase2 mpeg7_BAS ntl skin)
labels=(3 2 70 2 2)
kmax=(40 50 20 50 100)
c=(2048 512 2 128 2048)
g=(0.5 0.000122 0.0078125 8 0.5)
splits=(20 20 5 20 20)

rm -rf resultsA
mkdir resultsA

for i in 2
do
	echo "Running test for set ${datasets[$i]}" >> log.txt

	for (( j=1; j<=1; j++ ))
	do
		mkdir resultsA/${datasets[$i]}
		mkdir resultsA/${datasets[$i]}/opf
		mkdir resultsA/${datasets[$i]}/svm
		mkdir resultsA/${datasets[$i]}/knn1
		mkdir resultsA/${datasets[$i]}/knn3
		mkdir resultsA/${datasets[$i]}/knn5
		echo "Iteration $j" >> log.txt
		cp -f data/${datasets[$i]}.opf ${datasets[$i]}.opf
		./run_OPFA_on_dataset.sh ${datasets[$i]} ${labels[$i]} ${kmax[$i]} ${splits[$i]}
		./run_knnA_on_dataset.sh ${datasets[$i]} ${labels[$i]} 1 ${splits[$i]}
		./run_knnA_on_dataset.sh ${datasets[$i]} ${labels[$i]} 3 ${splits[$i]}
		./run_knnA_on_dataset.sh ${datasets[$i]} ${labels[$i]} 5 ${splits[$i]}
		./run_SVMA_on_dataset.sh ${datasets[$i]} ${labels[$i]} ${c[$i]} ${g[$i]} ${splits[$i]}
		mv -f resultsA/${datasets[$i]} resultsA/${datasets[$i]}$j
	done

done
