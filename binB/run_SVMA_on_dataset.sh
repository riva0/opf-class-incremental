# this script requires four arguments:
# $1: the dataset name (i.e. if running tests for 'dataset1.dat', the dataset name is 'dataset')
# $2: the number of classes in said dataset
# $3: cost of SVM
# $4: gamma of SVM
echo "Simulating experiment for $1, with $2 classes."

incsize=$5
initsize=$((100/incsize))


# Runs experiments where all classes begin as S_0
for (( i=1; i<=$2; i++ ))
do
	cp -f $1.opf $1.dat
	# Splits the dataset into S and T
	echo "	Splitting 'S' and 'T'..."
	{
		echo "./opf_split $1.dat 0.5 0 0.5 1"
		./opf_split $1.dat 0.5 0 0.5 1
		mv -f training.dat $1S.dat
		mv -f testing.dat $1T.dat
	} &>> /dev/null

	# Converts T to svm
	echo "	Converting T to SVM..."
	{
		./../tools/opf2txt $1T.dat $1T.txt
		./../tools/txt2svm $1T.txt $1T.svm
		rm -f $1T.txt
	} &>> /dev/null

	echo "	S_0 is of class $i"

	# Splits the S dataset into one file for each class
	echo "	Splitting classes"
	{
		echo "./opf_class_split $1S.dat 0"
		./opf_class_split $1S.dat 0
	} &>> /dev/null
	# Splits the initial class's file into two subsets; one is S_0, the other belongs to I
	echo "	Splitting initial class"
	{
		echo "./opf_split class$i.dat 0.5 0 0.5 0"
		./opf_split class$i.dat 0.5 0 0.5 0
	} &>> /dev/null
	mv -f training.dat $1S_0.dat
	mv -f testing.dat class$i.dat

	# build 1-class model on S_0 with SVM and test S_0 against T
	echo "	Initial training on S_0"
	{
		cp -f $1T.svm testS_0.svm
		./../tools/opf2txt $1S_0.dat $1S_0.txt
		./../tools/txt2svm $1S_0.txt $1S_0.svm
		rm -f $1S_0.txt
		echo "./svm-train -s 2 $1S_0.svm"
		./svm-train -s 2 $1S_0.svm
		mv svmtime.time trainS_0.svm.time
		echo "./svm-predict testS_0.svm $1S_0.svm.model out.svm"
		./svm-predict testS_0.svm $1S_0.svm.model out.svm
		mv svmtime.time testS_0.svm.time
		mv svmacc.acc testS_0.svm.acc
	} &>> /dev/null 

	# Running experiment with uniform increments
	# Merging all incremental datasets into $1I
	echo "	Merging class files..."
	{
		echo "./opf_merge class*.dat"
		./opf_merge class*.dat
	} &>> /dev/null
	mv -f merged.dat $1I.dat


	# Splitting $1I into increments
	echo "	Splitting '$1I' into increments..."
	{
		echo "./opf_incremental_split $1I.dat 1 0 0.$(printf "%02d" $initsize) $incsize 0"
		./opf_incremental_split $1I.dat 1 0 0.$(printf "%02d" $initsize) $incsize 0
	} &>> /dev/null

	cp $1S_0.dat merged.dat
	# Iterates through all of i segments
	for (( j=0; j<$incsize; j++ ))
	do
		echo "	Currently inc_training on II_$(printf "%04d" $j)"
		{	
			echo "./opf_merge merged.dat $1II_$(printf "%04d" $j).dat"
			./opf_merge merged.dat $1II_$(printf "%04d" $j).dat
			cp merged.dat $1II_$(printf "%04d" $j).dat

			cp -f $1T.svm testII_$(printf "%04d" $j).svm
			./../tools/opf2txt $1II_$(printf "%04d" $j).dat $1II_$(printf "%04d" $j).txt
			./../tools/txt2svm $1II_$(printf "%04d" $j).txt $1II_$(printf "%04d" $j).svm
			rm -f $1II_$(printf "%04d" $j).txt
			echo "./svm-train -c $3 -g $4 $1II_$(printf "%04d" $j).svm"
			./svm-train -c $3 -g $4 $1II_$(printf "%04d" $j).svm
			mv svmtime.time trainII_$(printf "%04d" $j).svm.time
			echo "./svm-predict testII_$(printf "%04d" $j).svm $1II_$(printf "%04d" $j).svm.model out.svm"
			./svm-predict testII_$(printf "%04d" $j).svm $1II_$(printf "%04d" $j).svm.model out.svm
			mv svmtime.time testII_$(printf "%04d" $j).svm.time
			mv svmacc.acc testII_$(printf "%04d" $j).svm.acc

		} &>> /dev/null
	done

	# finally, testing full training on 'S' against 'T'
	echo "	Full training..."
	{			
		cp -f $1T.svm testFull.svm
		./../tools/opf2txt $1S.dat $1S.txt
		./../tools/txt2svm $1S.txt $1S.svm
		rm -f $1S.txt
		echo "./svm-train -c $3 -g $4  $1S.svm"
		./svm-train -c $3 -g $4 $1S.svm
		mv svmtime.time trainFull.svm.time
		echo "./svm-predict testFull.svm $1S.svm.model out.svm"
		./svm-predict testFull.svm $1S.svm.model out.svm
		mv svmtime.time testFull.svm.time
		mv svmacc.acc testFull.svm.acc

	} &>> /dev/null

	echo "Outputting acc to acc file..."
	{
		echo "Full Accuracy"
		cat testFull.svm.acc

		echo "Accuracy - I"
		cat testS_0.svm.acc
		for (( j=0; j<$incsize; j++ ))
		do
			cat testII_$(printf "%04d" $j).svm.acc
		done


	} &> resultsA/$1/svm/accK$i.txt


	echo "Outputting time to time file..."
	{
		echo "Full Time (seconds)"
		cat testFull.svm.time

		echo "Incremental Time - I"
		cat testS_0.svm.time
		for (( j=0; j<$incsize; j++ ))
		do
			cat trainII_$(printf "%04d" $j).svm.time
		done

		echo "Classification Time - C"
		for (( j=0; j<$incsize; j++ ))
		do
			cat testII_$(printf "%04d" $j).svm.time
		done
	} &> resultsA/$1/svm/timeK$i.txt

	rm -f *.svm* *.dat*
done
