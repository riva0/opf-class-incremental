# this script requires three arguments:
# $1: the dataset name (i.e. if running tests for 'dataset1.dat', the dataset name is 'dataset')
# $2: the number of classes in said dataset
# $3: kmax for opf_cluster
echo "Simulating experiment for $1, with $2 classes. kmax = $3"

incsize=$4
initsize=$((100/incsize))


# Runs experiments where all classes begin as S_0
for (( i=1; i<=7; i++ ))
do
	cp -f $1.opf $1.dat
	# Splits the dataset into S and T
	echo "	Splitting 'S' and 'T'..."
	{
		echo "./opf_split $1.dat 0.5 0 0.5 1"
		./opf_split $1.dat 0.5 0 0.5 1
		mv -f training.dat $1S.dat
		mv -f testing.dat $1T.dat
	} &>> /dev/null

	echo "	S_0 is of class $i"

	# Splits the S dataset into one file for each class
	echo "	Splitting classes"
	{
		echo "./opf_class_split $1S.dat 0"
		./opf_class_split $1S.dat 0
	} &>> /dev/null
	# Splits the initial class's file into two subsets; one is S_0, the other belongs to I
	echo "	Splitting initial class"
	{
		echo "./opf_split class$i.dat 0.5 0 0.5 0"
		./opf_split class$i.dat 0.5 0 0.5 0
	} &>> /dev/null
	mv -f training.dat $1S_0.dat
	mv -f testing.dat class$i.dat

	# build 1-class model on S_0 with clustering tool and test S_0 against T
	echo "	Initial training on S_0"
	{
		echo "./opf_cluster $1S_0.dat $3 0 0"
		./opf_cluster $1S_0.dat $3 0 0
		cp -f $1T.dat testS_0.dat
		./opf_classify testS_0.dat
		./opf_accuracy testS_0.dat
	} &>> /dev/null

	# Running experiment with class-unique increments
	echo "Incremental-splitting each class file..."
	for (( j=1; j<=70; j++ ))
	do
		{
			mv -f class$j.dat $1$(printf "%d" $j)I.dat
			echo "./opf_incremental_split $1$(printf "%d" $j)I.dat 1 0 0.$(printf "%02d" $initsize) $incsize 0"
			./opf_incremental_split $1$(printf "%d" $j)I.dat 1 0 0.$(printf "%02d" $initsize) $incsize 0
		} &>> /dev/null

		# Iterates through all segments
		for (( k=0; k<$incsize; k++ ))
		do
			echo "	Currently inc_training on $(printf "%d" $j)II_$(printf "%04d" $k)"
			{	
				echo "./opf_includenew $1$(printf "%d" $j)II_$(printf "%04d" $k).dat $1$(printf "%d" $j)incremented.dat"
				./opf_includenew $1$(printf "%d" $j)II_$(printf "%04d" $k).dat $1$(printf "%d" $j)incremented.dat
				cp -f $1T.dat test$(printf "%d" $j)II_$(printf "%04d" $k).dat
				./opf_classify test$(printf "%d" $j)II_$(printf "%04d" $k).dat
				./opf_accuracy test$(printf "%d" $j)II_$(printf "%04d" $k).dat

			} &>> /dev/null
		done
	done
	

	# finally, testing full training on 'S' against 'T'
	echo "	Full training..."
	{
		echo "./opf_train $1S.dat"
		./opf_train $1S.dat
		cp -f $1T.dat testFull.dat
		./opf_classify testFull.dat
		./opf_accuracy testFull.dat
	} &>> /dev/null

	echo "Outputting acc to acc file..."
	{
		echo "Full Accuracy"
		cat testFull.dat.acc

		echo "Accuracy - I"
		cat testS_0.dat.acc
		for (( j=1; j<=70; j++ ))
		do
			for (( k=0; k<$incsize; k++ ))
			do
				cat test$(printf "%d" $j)II_$(printf "%04d" $k).dat.acc
			done
		done


	} &> resultsB/$1/opf/accK$i.txt


	echo "Outputting time to time file..."
	{
		echo "Full Time (seconds)"
		cat $1S.dat.time

		echo "Incremental Time - I"
		cat $1S_0.dat.time
		for (( j=1; j<=70; j++ ))
		do
			for (( k=0; k<$incsize; k++ ))
			do
				cat $1$(printf "%d" $j)II_$(printf "%04d" $k).dat.time
			done
		done

		echo "Classification Time - I"
		for (( j=1; j<=70; j++ ))
		do
			for (( k=0; k<$incsize; k++ ))
			do
				cat test$(printf "%d" $j)II_$(printf "%04d" $k).dat.time
			done
		done
	} &> resultsB/$1/opf/timeK$i.txt

	rm -f *.dat* classifier.opf
done