experiments = ["A", "B"]
datasets = ["cone-torus", "ntl", "SpamBase2", "Produce", "L3_4dist", "mpeg7_BAS"]#, "skin"]
labels = [[1], [1], [1], [1], [1], [1]]
classifiers = ["knn3", "knn5", "opf", "svm"]#["knn1","knn3", "knn5", "opf", "svm"]
iterations = range(1,11)

import matplotlib.pyplot as plt
from itertools import cycle

def isNumber(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def getLabel(c):
	if (c[0:3] == "knn"):
		return c[3] + "-NN"
	else: 
		return c.upper()

#color_list = ["r","g","b","c","m"]
color_list = [("r", "o"), ("g", "D"), ("b", "p"), ("k", "*"), ("m", "^")]
#color_list = [("0.6", "o"), ("0.45", "p"), ("0.3", "D"), ("0.15", "*"), ("0.0", "^")]
#color_list = [("0.45", "p"), ("0.3", "D"), ("0.15", "*"), ("0.0", "^")]
color_cycle = cycle(color_list)
style_list = ["-",":","-."]
style_cycle = cycle(style_list)

for experiment in experiments:
	for index, dataset in enumerate(datasets):
		fig = plt.figure()

		style_cycle = cycle(style_list)
		for label in labels[index]:
			style = next(style_cycle)
			color_cycle = cycle(color_list)
			for classifier in classifiers:
				color = next(color_cycle)
				if (classifier[0:3] == "knn"):
					isKNN = True
				else:
					isKNN = False
				print("Currently at experiment " + experiment + ", classifier " + classifier + ", dataset " + dataset + ", label " + str(label))

				inFile = open(experiment+"_"+classifier+"_"+dataset+"_K"+str(label)+".csv", "r")

				# Reads the first line, which is a header, and discards
				line = next(inFile)
				# Second line is the full values; to be plotted as an "upper bound"
				line = next(inFile)
				mean_full_acc, stdev_full_acc, mean_full_time, stdev_full_time = map(float, line.split(','))

				# Preparing the value vectors. Not Pythonesque. Not caring.
				accuracies = []
				I_times = []
				C_times = [[0,0]]

				# Now reads sets of accuracies and times. They are bounded by lines beginning with "mean". Still don't care.
				readingState = "X"; readingStates = ["Accuracy","I_times","C_times"]; i =0
				for line in inFile:
					if line[0:4] != "mean":
						if readingState is 'Accuracy':
							accuracies.append(list(map(float, line.split(','))))
						elif readingState is 'I_times':
							I_times.append(list(map(float, line.split(','))))
						elif readingState is 'C_times':
							C_times.append(list(map(float, line.split(','))))
					else:
						readingState = readingStates[i]
						i = i + 1

				# Has all values, goodie! Now we PLOT
				colorNow, markerNow = color
				plt.subplot(111) # Plotting accuracy
				# First, plot an horizontal line for the full training set
				plt.plot(range(len(accuracies)),[mean_full_acc] * len(accuracies), color=colorNow, linestyle=":", marker=markerNow, markersize=9.0, markevery=0.1)#, label="Full {}".format(classifier, label))
				# Now plot all accuracy values
				plt.plot(range(len(accuracies)),[x[0] for x in accuracies], color=colorNow, marker=markerNow, markersize=9.0, markevery=0.1, label="{}".format(getLabel(classifier)))

				#plt.subplot(111) # Plotting time
				if (not isKNN):
					final_times = [I_times[i][0]] + [I_times[i][0] + C_times[i-1][0] for i in range(1,len(I_times))]
				else:
					final_times = [x[0] for x in I_times]
				#plt.plot(range(len(final_times)), final_times, color=colorNow, marker=markerNow, markersize=9.0, markevery=0.1, label="{}".format(getLabel(classifier)))



		# Experiment + dataset level. We're making one plot per experiment per dataset, grouping classifiers and labels.
		plt.subplot(111) # Subplot 1 is Accuracy.
		plt.title("Balanced accuracy of classifiers for experiment {} on dataset {}".format(experiment, dataset))
		plt.xlabel('Iteration')
		plt.ylabel('Balanced Accuracy')
		plt.legend(loc=4, ncol=2)
		plt.axis([0, len(accuracies)-1, min([x[0] for x in accuracies]), 100])
			
		#plt.subplot(111) # Subplot 2 is times.
		#plt.title("Running time of classifiers for experiment {} on dataset {}".format(experiment, dataset))
		#plt.xlabel('Iteration')
		#plt.ylabel('Time (seconds)')
		#plt.legend(loc=2, ncol=2)
		#plt.xlim(0, len(final_times)-1)

		# figsave
		#plt.show()
		fig.tight_layout()
		fig.savefig("acc_"+experiment+"_"+dataset+".png")



#color_list = ["r","g","b","c","m"]
color_list = [("r", "o"), ("g", "D"), ("b", "p"), ("k", "*"), ("m", "^")]
#color_list = [("0.6", "o"), ("0.45", "p"), ("0.3", "D"), ("0.15", "*"), ("0.0", "^")]
#color_list = [("0.45", "p"), ("0.3", "D"), ("0.15", "*"), ("0.0", "^")]
color_cycle = cycle(color_list)
style_list = ["-",":","-."]
style_cycle = cycle(style_list)

for experiment in experiments:
	for index, dataset in enumerate(datasets):
		fig = plt.figure()

		style_cycle = cycle(style_list)
		for label in labels[index]:
			style = next(style_cycle)
			color_cycle = cycle(color_list)
			for classifier in classifiers:
				color = next(color_cycle)
				if (classifier[0:3] == "knn"):
					isKNN = True
				else:
					isKNN = False
				print("Currently at experiment " + experiment + ", classifier " + classifier + ", dataset " + dataset + ", label " + str(label))

				inFile = open(experiment+"_"+classifier+"_"+dataset+"_K"+str(label)+".csv", "r")

				# Reads the first line, which is a header, and discards
				line = next(inFile)
				# Second line is the full values; to be plotted as an "upper bound"
				line = next(inFile)
				mean_full_acc, stdev_full_acc, mean_full_time, stdev_full_time = map(float, line.split(','))

				# Preparing the value vectors. Not Pythonesque. Not caring.
				accuracies = []
				I_times = []
				C_times = [[0,0]]

				# Now reads sets of accuracies and times. They are bounded by lines beginning with "mean". Still don't care.
				readingState = "X"; readingStates = ["Accuracy","I_times","C_times"]; i =0
				for line in inFile:
					if line[0:4] != "mean":
						if readingState is 'Accuracy':
							accuracies.append(list(map(float, line.split(','))))
						elif readingState is 'I_times':
							I_times.append(list(map(float, line.split(','))))
						elif readingState is 'C_times':
							C_times.append(list(map(float, line.split(','))))
					else:
						readingState = readingStates[i]
						i = i + 1

				# Has all values, goodie! Now we PLOT
				colorNow, markerNow = color
				plt.subplot(111) # Plotting accuracy
				# First, plot an horizontal line for the full training set
				#plt.plot(range(len(accuracies)),[mean_full_acc] * len(accuracies), color=colorNow, linestyle=":", marker=markerNow, markersize=9.0, markevery=0.1)#, label="Full {}".format(classifier, label))
				# Now plot all accuracy values
				#plt.plot(range(len(accuracies)),[x[0] for x in accuracies], color=colorNow, marker=markerNow, markersize=9.0, markevery=0.1, label="{}".format(getLabel(classifier)))

				#plt.subplot(111) # Plotting time
				if (not isKNN):
					final_times = [I_times[i][0]] + [I_times[i][0] + C_times[i-1][0] for i in range(1,len(I_times))]
				else:
					final_times = [x[0] for x in I_times]
				plt.plot(range(len(final_times)), final_times, color=colorNow, marker=markerNow, markersize=9.0, markevery=0.1, label="{}".format(getLabel(classifier)))



		# Experiment + dataset level. We're making one plot per experiment per dataset, grouping classifiers and labels.
		#plt.subplot(111) # Subplot 1 is Accuracy.
		#plt.title("Balanced accuracy of classifiers for experiment {} on dataset {}".format(experiment, dataset))
		#plt.xlabel('Iteration')
		#plt.ylabel('Balanced Accuracy')
		#plt.legend(loc=4, ncol=2)
		#plt.axis([0, len(accuracies)-1, min([x[0] for x in accuracies]), 100])
			
		plt.subplot(111) # Subplot 2 is times.
		plt.title("Running time of classifiers for experiment {} on dataset {}".format(experiment, dataset))
		plt.xlabel('Iteration')
		plt.ylabel('Time (seconds)')
		plt.legend(loc=2, ncol=2)
		plt.xlim(0, len(final_times)-1)

		# figsave
		#plt.show()
		fig.tight_layout()
		fig.savefig("time_"+experiment+"_"+dataset+".png")