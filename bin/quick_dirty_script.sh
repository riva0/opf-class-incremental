#!/bin/bash
for i in cone-torus SpamBase2 mpeg7_BAS ntl L3_4dist Produce
do
	./opf_split "$i".opf 0.5 0 0.5 1
	./opf_merge training.dat testing.dat
	./../tools/opf2txt merged.dat "$i".txt
	./../tools/txt2svm "$i".txt "$i".svm
	mv -f "$i".svm "../other classifiers/libsvm-3.21"

done
