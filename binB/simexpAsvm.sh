#!/bin/bash
datasets=(cone-torus SpamBase2 ntl skin)
labels=(3 2 2 2)
c=(2048 512 2 2048)
g=(0.5 0.000122 0.0078125 0.5)


mkdir resultsA

for i in 2 3 
do
	echo "Running test for set ${datasets[$i]}" >> log.txt

	for (( j=1; j<=10; j++ ))
	do
		mkdir resultsA/${datasets[$i]}
		mkdir resultsA/${datasets[$i]}/svm
		echo "Iteration $j" >> log.txt
		cp -f data/${datasets[$i]}.opf ${datasets[$i]}.opf
		./run_SVMA_on_dataset.sh ${datasets[$i]} ${labels[$i]} ${c[$i]} ${g[$i]}
		mv -f resultsA/${datasets[$i]} resultsA/${datasets[$i]}$j
	done

done
