LIB=./lib
INCLUDE=./include
SRC=./src
OBJ=./obj
UTIL=util

CC=gcc

FLAGS=  -O3 -g -Wall


INCFLAGS = -I$(INCLUDE) -I$(INCLUDE)/$(UTIL)

all: libOPF opf_train opf_cluster opf_includenew opf_includedift opf_classify opf_accuracy opf_classifyscore opf_split opf_incremental_split opf_class_split opf_merge opf_distance opf_info statistics format_tools base_creator

libOPF: libOPF-build
	echo "libOPF.a built..."

libOPF-build: \
util \
$(OBJ)/OPF.o \
$(OBJ)/OPFincremental.o \
$(OBJ)/OPFactive.o \

	ar csr $(LIB)/libOPF.a \
$(OBJ)/common.o \
$(OBJ)/set.o \
$(OBJ)/diversity.o \
$(OBJ)/gqueue.o \
$(OBJ)/realheap.o \
$(OBJ)/sgctree.o \
$(OBJ)/subgraph.o \
$(OBJ)/OPF.o \
$(OBJ)/OPFincremental.o \
$(OBJ)/OPFactive.o \

$(OBJ)/OPF.o: $(SRC)/OPF.c
	$(CC) $(FLAGS) -g -c $(SRC)/OPF.c $(INCFLAGS) \
	-o $(OBJ)/OPF.o

$(OBJ)/OPFincremental.o: $(SRC)/OPFincremental.c
	$(CC) $(FLAGS) -Wall -g -c $(SRC)/OPFincremental.c $(INCFLAGS) \
	-o $(OBJ)/OPFincremental.o

$(OBJ)/OPFactive.o: $(SRC)/OPFactive.c
	$(CC) $(FLAGS) -Wall -g -c $(SRC)/OPFactive.c $(INCFLAGS) \
	-o $(OBJ)/OPFactive.o

opf_split: libOPF
	$(CC) $(FLAGS) $(INCFLAGS) src/opf_split.c  -L./lib -o bin/opf_split -lOPF -lm

opf_incremental_split: libOPF
	$(CC) $(FLAGS) $(INCFLAGS) src/opf_incremental_split.c  -L./lib -o bin/opf_incremental_split -lOPF -lm

opf_class_split: libOPF
	$(CC) $(FLAGS) $(INCFLAGS) src/opf_class_split.c  -L./lib -o bin/opf_class_split -lOPF -lm

opf_merge: libOPF
	$(CC) $(FLAGS) $(INCFLAGS) src/opf_merge.c  -L./lib -o bin/opf_merge -lOPF -lm

opf_accuracy: libOPF
	$(CC) $(FLAGS) $(INCFLAGS) src/opf_accuracy.c  -L./lib -o bin/opf_accuracy -lOPF -lm

opf_train: libOPF
	$(CC) $(FLAGS) $(INCFLAGS) src/opf_train.c  -L./lib -o bin/opf_train -lOPF -lm

opf_classify: libOPF
	$(CC) $(FLAGS) $(INCFLAGS) src/opf_classify.c  -L./lib -o bin/opf_classify -lOPF -lm

opf_classifyscore: libOPF
	$(CC) $(FLAGS) $(INCFLAGS) src/opf_classifyscore.c  -L./lib -o bin/opf_classifyscore -lOPF -lm

opf_distance: libOPF
	$(CC) $(FLAGS) $(INCFLAGS) src/opf_distance.c  -L./lib -o bin/opf_distance -lOPF -lm

opf_info: libOPF
	$(CC) $(FLAGS) $(INCFLAGS) src/opf_info.c  -L./lib -o bin/opf_info -lOPF -lm

opf_cluster: libOPF
	$(CC) $(FLAGS) $(INCFLAGS) src/opf_cluster.c  -L./lib -o bin/opf_cluster -lOPF -lm

opf_includenew: libOPF
	$(CC) $(FLAGS) $(INCFLAGS) -g src/opf_includenew.c -L./lib -o bin/opf_includenew -lOPF -lm

opf_includedift: libOPF
	$(CC) $(FLAGS) $(INCFLAGS) -g src/opf_includedift.c -L./lib -o bin/opf_includedift -lOPF -lm

statistics:
	$(CC) $(FLAGS) tools/src/statistics.c  -o tools/statistics -lm

format_tools: libOPF
	$(CC) $(FLAGS) $(INCFLAGS) tools/src/txt2opf.c  -L./lib -o tools/txt2opf -lOPF -lm
	$(CC) $(FLAGS) $(INCFLAGS) tools/src/opf2txt.c  -L./lib -o tools/opf2txt -lOPF -lm
	$(CC) $(FLAGS) $(INCFLAGS) tools/src/txt2svm.c  -L./lib -o tools/txt2svm -lOPF -lm
	$(CC) $(FLAGS) $(INCFLAGS) tools/src/svm2txt.c  -L./lib -o tools/svm2txt -lOPF -lm

base_creator: libOPF
	$(CC) $(FLAGS) $(INCFLAGS) tools/src/base_creator.c  -L./lib -o tools/base_creator -lOPF -lm

util: $(SRC)/$(UTIL)/common.c $(SRC)/$(UTIL)/set.c $(SRC)/$(UTIL)/diversity.c $(SRC)/$(UTIL)/gqueue.c $(SRC)/$(UTIL)/realheap.c $(SRC)/$(UTIL)/sgctree.c $(SRC)/$(UTIL)/subgraph.c
	$(CC) $(FLAGS) $(INCFLAGS) -c $(SRC)/$(UTIL)/common.c -o $(OBJ)/common.o
	$(CC) $(FLAGS) $(INCFLAGS) -c $(SRC)/$(UTIL)/set.c -o $(OBJ)/set.o
	$(CC) $(FLAGS) $(INCFLAGS) -c $(SRC)/$(UTIL)/diversity.c -o $(OBJ)/diversity.o
	$(CC) $(FLAGS) $(INCFLAGS) -c $(SRC)/$(UTIL)/gqueue.c -o $(OBJ)/gqueue.o
	$(CC) $(FLAGS) $(INCFLAGS) -c $(SRC)/$(UTIL)/realheap.c -o $(OBJ)/realheap.o
	$(CC) $(FLAGS) $(INCFLAGS) -c $(SRC)/$(UTIL)/sgctree.c -o $(OBJ)/sgctree.o
	$(CC) $(FLAGS) $(INCFLAGS) -c $(SRC)/$(UTIL)/subgraph.c -o $(OBJ)/subgraph.o



## Cleaning-up

clean:
	rm -f $(LIB)/lib*.a; rm -f $(OBJ)/*.o bin/opf_split bin/opf_incremental_split bin/opf_accuracy bin/opf_includedift bin/opf_train bin/opf_classify bin/opf_distance bin/opf_class_split bin/opf_info bin/opf_cluster tools/statistics tools/txt2opf bin/opf_includenew bin/opf_classifyscore

clean_results:
	rm -f *.out *.opf *.acc *.time *.opf training.dat evaluating.dat testing.dat

clean_results_in_examples:
	rm -f examples/*.out examples/*.opf examples/*.acc examples/*.time examples/*.opf examples/training.dat examples/evaluating.dat examples/testing.dat


