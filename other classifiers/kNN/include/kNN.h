#ifndef _KNN_H_
#define _KNN_H_

#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <math.h>
#include <float.h>
#include <assert.h>
#include <sys/time.h>
#include <time.h>

#include "common.h"
#include "set.h"
#include "gqueue.h"
#include "subgraph.h"
#include "sgctree.h"
#include "realheap.h"

/*--------- Common definitions --------- */
#define knn_MAXARCW			100000.0
#define knn_NORMAL			1
#define knn_ANOMALY			2

// Compute accuracy
float knn_Accuracy(Subgraph *sg);

//	kNN classification
void knn_kNNClassifying(Subgraph *sgtrain, Subgraph* sg, int k);

// Compute Euclidean distance between feature vectors
float knn_EuclDist(float *f1, float *f2, int n);
// Discretizes original distance
float knn_EuclDistLog(float *f1, float *f2, int n);

#endif