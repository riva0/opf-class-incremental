experiments = ["A", "B"]
datasets = ["L3_4dist"]#, "ntl", "SpamBase2"]#, "skin"]
labels = [[1]]#, range(1,3), range(1,3)]#, range(1,3)]
classifiers = ["1-NN", "3-NN", "5-NN", "OPF", "SVM"]
iterations = range(1,11)

import matplotlib.pyplot as plt
from itertools import cycle
import numpy as np

def isNumber(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

#color_list = ["-^","-k*","-kx","-kD","-ko"]
color_list = [("0.6", "o"), ("0.45", "x"), ("0.3", "D"), ("0.15", "*"), ("0.0", "^")]
color_cycle = cycle(color_list)
#style_list = ["-",":","-."]
style_list = [""]
style_cycle = cycle(style_list)

for index, dataset in enumerate(datasets):
	#try:
	fig = plt.figure()

	style_cycle = cycle(style_list)
	for label in labels[index]:
		style = next(style_cycle)
		color_cycle = cycle(color_list)
		for classifier in classifiers:
			color = next(color_cycle)
			if (classifier[2:4] == "NN"):
				isKNN = True
			else:
				isKNN = False
			print("Currently at classifier " + classifier + ", dataset " + dataset + ", label " + str(label))

			inFile = open(classifier+"_"+dataset+"_K"+str(label)+".csv", "r")

			# Reads the first line, which is a header, and discards
			line = next(inFile)
			# Second line is the full values; to be plotted as an "upper bound"
			line = next(inFile)
			mean_full_acc, stdev_full_acc, mean_full_time, stdev_full_time = map(float, line.split(','))

			# Preparing the value vectors. Not Pythonesque. Not caring.
			accuracies = []
			I_times = []
			C_times = [[0,0]]

			# Now reads sets of accuracies and times. They are bounded by lines beginning with "mean". Still don't care.
			readingState = "X"; readingStates = ["Accuracy","I_times","C_times"]; i =0
			for line in inFile:
				if line[0:4] != "mean":
					if readingState is 'Accuracy':
						accuracies.append(list(map(float, line.split(','))))
					elif readingState is 'I_times':
						I_times.append(list(map(float, line.split(','))))
					elif readingState is 'C_times':
						C_times.append(list(map(float, line.split(','))))
				else:
					readingState = readingStates[i]
					i = i + 1

			# Has all values, goodie! Now we PLOT
			#plt.subplot(121) # Plotting accuracy
			# First, plot an horizontal line for the full training set
			#plt.plot(range(len(accuracies)),[mean_full_acc] * len(accuracies), color+"--")#, label="Full {}, k{}".format(classifier, label))
			# Now plot all accuracy values
			colorNow, markerNow = color
			#plt.plot(range(len(accuracies)),[x[0] for x in accuracies], color=colorNow, marker=markerNow, label="{}".format(classifier))

			ax = plt.subplot(111) # Plotting time
			if (not isKNN):
				final_times = [I_times[i][0] + C_times[i][0] for i in range(len(I_times))]
			else:
				final_times = [x[0] for x in I_times]
			final_times = np.cumsum(final_times)
			ax.set_yscale('log')
			plt.plot(range(len(final_times)), final_times, color=colorNow, marker=markerNow, label="{}".format(classifier))



	# Experiment + dataset level. We're making one plot per experiment per dataset, grouping classifiers and labels.
	#plt.subplot(121) # Subplot 1 is Accuracy.
	#plt.title("Balanced accuracy of classifiers on dataset {}".format(dataset))
	#plt.xlabel('Iteration')
	#plt.ylabel('Balanced Accuracy')
	#plt.legend(loc=4, ncol=2, fontsize='x-small')
	#plt.axis([0, len(accuracies)-1, min([x[0] for x in accuracies],50), 100])
		
	#plt.subplot(122) # Subplot 2 is times.
	plt.title("Running time of classifiers on dataset {}".format(dataset))
	plt.xlabel('Iteration')
	plt.ylabel('Time (seconds)')
	plt.legend(loc=2, ncol=2, fontsize='x-small')
	plt.xlim(0, len(final_times)-1)

	# figsave
	#plt.show()
	fig.savefig(dataset+".png")
	#except:
		#print "An error has ocurred! {}, {}, {}".format(dataset, classifier, label)