#include "kNN.h"

int main(int argc, char **argv){
	fflush(stdout);
	fprintf(stdout, "\nProgram that executes the test phase of a simple kNN classifier\n");
	fprintf(stdout, "\nIf you have any problems, please contact: ");
	fprintf(stdout, "\n- riva.mateus@gmail.com");
	fprintf(stdout, "\n\nOPF Anomaly Detector 0.1 (2016)\n");
	fprintf(stdout, "\n"); fflush(stdout);

	if((argc < 4)){
		fprintf(stderr, "\nusage knn_classify <P1> <P2> <P3>");
		fprintf(stderr, "\nP1: training set in the OPF file format");
		fprintf(stderr, "\nP2: test set in the OPF file format");
		fprintf(stderr, "\nP3: number of neighbours to compute ('k')");
		exit(-1);
	}

	int i;
	float time;
	char fileName[256];
	FILE *f = NULL;
	timer tic, toc;

	// Parsing arguments
	int k = atoi(argv[3]);

	fprintf(stdout, "\nReading data files ..."); fflush(stdout);
	Subgraph *gTest = ReadSubgraph(argv[2]), *gTrain = ReadSubgraph(argv[1]);
	fprintf(stdout, " OK"); fflush(stdout);

	fprintf(stdout, "\nClassifying test set ..."); fflush(stdout);
	gettimeofday(&tic,NULL);
	knn_kNNClassifying(gTrain, gTest, k); gettimeofday(&toc,NULL);
	fprintf(stdout, " OK"); fflush(stdout);

	fprintf(stdout, "\nCalculating accuracy ..."); fflush(stdout);
	float accuracy = knn_Accuracy(gTest);
	int correct = 0;
	for (i = 0; i < gTest->nnodes; i++)
		if (gTest->node[i].label == gTest->node[i].truelabel) correct++;
	
	fprintf(stdout, " OK"); fflush(stdout);
	fprintf(stdout, "\tAccuracy: %f (%d correct of %d)", accuracy, correct, gTest->nnodes);

	fprintf(stdout, "\nWriting output file ..."); fflush(stdout);
	sprintf(fileName,"%s.out",argv[2]);
	f = fopen(fileName,"w");
	for (i = 0; i < gTest->nnodes; i++){
		fprintf(f,"%d %d\n",gTest->node[i].label,gTest->node[i].truelabel);
	}
	fclose(f);
	fprintf(stdout, " OK"); fflush(stdout);

	fprintf(stdout, "\nWriting accuracy file ..."); fflush(stdout);
	sprintf(fileName,"%s.acc",argv[2]);
	f = fopen(fileName,"a");
	fprintf(f,"%f\n",accuracy*100);
	fclose(f);
	fprintf(stdout, " OK"); fflush(stdout);

	fprintf(stdout, "\nDeallocating memory ...");
	DestroySubgraph(&gTrain);
	DestroySubgraph(&gTest);
	fprintf(stdout, " OK\n");

	time = ((toc.tv_sec-tic.tv_sec)*1000.0 + (toc.tv_usec-tic.tv_usec)*0.001)/1000.0;
	fprintf(stdout, "\nTesting time: %f seconds\n", time); fflush(stdout);

	sprintf(fileName,"%s.time",argv[2]);
	f = fopen(fileName,"a");
	fprintf(f,"%f\n",time);
	fclose(f);

	return 0;
}
