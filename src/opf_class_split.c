#include "OPFactive.h"

int main(int argc, char **argv){
	fflush(stdout);
	fprintf(stdout, "\nProgram that splits a dataset's classes into different subdatasets\n");
	fprintf(stdout, "\nIf you have any problem, please contact: ");
	fprintf(stdout, "\n- riva.mateus@gmail.com");
	fprintf(stdout, "\n- moacirponti@gmail.com\n");
	fprintf(stdout, "\nLibOPFal version 1.0 (2016)\n");
	fprintf(stdout, "\n"); fflush(stdout);

	if(argc != 3){
		fprintf(stderr, "\nusage opf_class_split <P1> <P2>");
		fprintf(stderr, "\nP1: input dataset in the OPF file format");
		fprintf(stderr, "\nP2: normalize features? 1 - Yes  0 - No\n\n");
		exit(-1);
	}
	Subgraph *g = NULL, **subG = NULL;
	int normalize = atoi(argv[2]);

	int i;

	// Reading initial subgraph file
	fprintf(stdout, "\nReading data set ..."); fflush(stdout);
	g = ReadSubgraph(argv[1]);
	fprintf(stdout, " OK"); fflush(stdout);

	// If required, normalize features
	if(normalize) opf_NormalizeFeatures(g);

	fprintf(stdout, "\nSplitting data set ..."); fflush(stdout);
	subG = opf_SplitSubgraphClasses(g);
	fprintf(stdout, " OK"); fflush(stdout);

	fprintf(stdout, "\nWriting data sets to disk ..."); fflush(stdout);
	for(i = 0; i < g->nlabels; i++){
		char filename[64];
		sprintf(filename,"class%d.dat", i+1);
		WriteSubgraph(subG[i], filename);
	}
	fprintf(stdout, " OK"); fflush(stdout);

	fprintf(stdout, "\nDeallocating memory ...");
	for(i = 0; i < g->nlabels; i++)
		DestroySubgraph(&(subG[i]));
	free(subG);
	DestroySubgraph(&g);
	fprintf(stdout, " OK\n");

	return 0;
}
