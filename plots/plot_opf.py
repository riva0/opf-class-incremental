import matplotlib.pyplot as plt
import sys

def isNumber(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

for filename in ["1","2","3","4","5", "6"]:
	inFile = open(filename+".plt")

	data = []
	colorAvailable = ['r','g','b']

	for line in inFile:
		example = map(float, line.split())
		data.append(example)

	# setting plot up
	fig = plt.figure()
	plt.axis([-3, 3, -3, 3])

	# plot all points, colored by label
	for example in data:
		if example[3] >= 0:
			plt.plot(example[0], example[1], colorAvailable[int(example[2])-1]+'o')
		else:
			plt.plot(example[0], example[1], colorAvailable[int(example[2])-1]+'d')

	# plot predecessor lines
	for example in data:
		if (example[3] >= 0):
			x = [example[0], data[int(example[3])][0]]
			y = [example[1], data[int(example[3])][1]]
			plt.plot(x,y,'k-')

	#plt.show()
	fig.savefig(filename+".png")