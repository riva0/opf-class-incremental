#experiments = ["A", "B"]
experiments = ["_case_study"]
datasets = ["L3_4dist"]#, "ntl", "SpamBase2"]#, "skin"]
labels = [[1]]#, range(1,3), range(1,3)]#, range(1,3)]
classifiers = ["knn1", "knn3", "knn5", "opf", "svm"]
iterations = [""]#range(1,11)

import statistics

def isNumber(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

#for experiment in experiments:
for classifier in classifiers:
	if (classifier[0:3] == "knn"):
		isKNN = True
	else:
		isKNN = False
	for index, dataset in enumerate(datasets):
		for label in labels[index]:
			print("Currently at classifier " + classifier + ", dataset " + dataset + ", label " + str(label))

			Iaccuracies = []; Itimes = []; 
			Ctimes = []
			FULLaccuracies = []; FULLtimes = []
			IMEAN_accuracies = []; IMEAN_times = []
			CMEAN_times = []
			ISTDEVaccuracies = []; ISTDEVtimes = []
			CSTDEVtimes = []

			accFile = open(dataset+"/"+classifier+"/"+"accK"+str(label)+".txt", "r")
			timeFile =  open(dataset+"/"+classifier+"/"+"timeK"+str(label)+".txt", "r")

			iterationIaccuracies = []
			iterationFULLaccuracy = 0
			iterationItimes = []
			iterationCtimes = []
			iterationFULLtime = 0

			# reading accuracies
			readingState = 'X'; readingStates = ['F','I']; i =0
			for line in accFile:
				if isNumber(line):
					if readingState is 'F':
						iterationFULLaccuracy = float(line)
					elif readingState is 'I':
						iterationIaccuracies.append(float(line))
				else:
					if line[0:3] != "cat":
						readingState = readingStates[i]
						i = i + 1

			# reading times
			readingState = 'X'; readingStates = ['F','I','C']; i =0
			for line in timeFile:
				if isNumber(line):
					if readingState is 'F':
						iterationFULLtime = float(line)
					elif readingState is 'I':
						iterationItimes.append(float(line))
					elif readingState is 'C':
						iterationCtimes.append(float(line))
				else:
					if line[0:3] != "cat":
						readingState = readingStates[i]
						i = i + 1
			Iaccuracies.append(iterationIaccuracies)
			FULLaccuracies.append(iterationFULLaccuracy)

			Itimes.append(iterationItimes)
			FULLtimes.append(iterationFULLtime)
			if (not isKNN): Ctimes.append(iterationCtimes)

			# Read everything, now find means and stddevs
			for line in range(len(Iaccuracies[0])):
				meanIAccList = []; 
				
				if line < (len(Iaccuracies[0])):
					meanIAccList.append(Iaccuracies[0][line])

				try:
					IMEAN_accuracies.append(statistics.mean(meanIAccList))
				except statistics.StatisticsError:
					IMEAN_accuracies.append(meanIAccList[0])
				try:
					ISTDEVaccuracies.append(statistics.stdev(meanIAccList))
				except statistics.StatisticsError:
					ISTDEVaccuracies.append(0.0)

				
			meanFULLAccList = []; meanFULLTimeList = [];
			
			meanFULLAccList.append(FULLaccuracies[0])
			meanFULLTimeList.append(FULLtimes[0])

			FULLMEAN_accuracy = statistics.mean(meanFULLAccList)
			FULLSTDEVaccuracy = 0
			FULLMEAN_time = statistics.mean(meanFULLTimeList)
			FULLSTDEVtime = 0

			for line in range(len(Itimes[0])):
				meanITimeList = []

				
				if line < (len(Itimes[0])):
					meanITimeList.append(Itimes[0][line])

				try:
					IMEAN_times.append(statistics.mean(meanITimeList))
				except statistics.StatisticsError:
					IMEAN_times.append(meanITimeList[0])
				try:
					ISTDEVtimes.append(statistics.stdev(meanITimeList))
				except statistics.StatisticsError:
					ISTDEVtimes.append(0.0)

			if (not isKNN):	
				for line in range(len(Ctimes[0])):
					meanCTimeList = []

					if line < (len(Ctimes[0])):
						meanCTimeList.append(Ctimes[0][line])

					try:
						CMEAN_times.append(statistics.mean(meanCTimeList))
					except statistics.StatisticsError:
						CMEAN_times.append(meanCTimeList[0])
					try:
						CSTDEVtimes.append(statistics.stdev(meanCTimeList))
					except statistics.StatisticsError:
						CSTDEVtimes.append(0.0)


			outFile = open(classifier+"_"+dataset+"_K"+str(label)+".csv", "w+")

			outFile.write("mean_accFULL,stdev_accFULL, mean_timeFULL, stdev_timeFULL\n")
			outFile.write(str(FULLMEAN_accuracy)+","+str(FULLSTDEVaccuracy)+","+str(FULLMEAN_time)+","+str(FULLSTDEVtime) + "\n")
			
			outFile.write("mean_accI,stdev_accI\n")
			for i in range(len(IMEAN_accuracies)):
				outFile.write(str(IMEAN_accuracies[i]) + "," + str(ISTDEVaccuracies[i]) + "\n")

			outFile.write("mean_timeI,stdev_timeI\n")
			for i in range(len(IMEAN_times)):
				outFile.write(str(IMEAN_times[i]) + "," + str(ISTDEVtimes[i]) + "\n")

			if (not isKNN):					
				outFile.write("mean_timeC,stdev_timeC\n")
				for i in range(len(CMEAN_times)):
					outFile.write(str(CMEAN_times[i]) + "," + str(CSTDEVtimes[i]) + "\n")