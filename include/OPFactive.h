#ifndef _OPF_AL_H_
#define _OPF_AL_H_

#include "OPF.h"
#include "OPFincremental.h"

#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <math.h>
#include <float.h>
#include <assert.h>
#include <sys/time.h>
#include <time.h>

#include "common.h"
#include "set.h"
#include "gqueue.h"
#include "subgraph.h"
#include "sgctree.h"
#include "realheap.h"

/*----------- OPF-Active Learning Code ------------------------------*/

// Split subgraph into multiple parts, one for each class.
Subgraph** opf_SplitSubgraphClasses(Subgraph *sg);

#endif