#!/bin/bash
datasets=(skin)
labels=(2)
kmax=(100)

rm -rf resultsA
mkdir resultsA

for i in 0
do
	echo "Running test for set ${datasets[$i]}" >> log.txt

	for (( j=1; j<=10; j++ ))
	do
		mkdir resultsA/${datasets[$i]}
		mkdir resultsA/${datasets[$i]}/opf
		#mkdir resultsA/${datasets[$i]}/svm
		mkdir resultsA/${datasets[$i]}/knn1
		mkdir resultsA/${datasets[$i]}/knn3
		mkdir resultsA/${datasets[$i]}/knn5
		echo "Iteration $j" >> log.txt
		cp -f data/${datasets[$i]}.opf ${datasets[$i]}.opf
		./run_OPFA_on_dataset.sh ${datasets[$i]} ${labels[$i]} ${kmax[$i]}
		./run_knnA_on_dataset.sh ${datasets[$i]} ${labels[$i]} 1
		./run_knnA_on_dataset.sh ${datasets[$i]} ${labels[$i]} 3
		./run_knnA_on_dataset.sh ${datasets[$i]} ${labels[$i]} 5
		#./run_SVMA_on_dataset.sh ${datasets[$i]} ${labels[$i]}
		mv -f resultsA/${datasets[$i]} resultsA/${datasets[$i]}$j
	done

done
