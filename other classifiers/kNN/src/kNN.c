#include "kNN.h"

// Compute accuracy
float knn_Accuracy(Subgraph *sg){
	float Acc = 0.0f, **error_matrix = NULL, error = 0.0f;
	int i, *nclass = NULL, nlabels=0;

	error_matrix = (float **)calloc(sg->nlabels+1, sizeof(float *));
	for(i=0; i<= sg->nlabels; i++)
	  error_matrix[i] = (float *)calloc(2, sizeof(float));

	nclass = (int*)calloc((sg->nlabels+1), sizeof(int));

	for (i = 0; i < sg->nnodes; i++){
	  nclass[sg->node[i].truelabel]++;
	}

	for (i = 0; i < sg->nnodes; i++){
	  if(sg->node[i].truelabel != sg->node[i].label){
	    error_matrix[sg->node[i].truelabel][1]++;
	    error_matrix[sg->node[i].label][0]++;
	  }
	}

	for(i=1; i <= sg->nlabels; i++){
	  if (nclass[i]!=0){
	    error_matrix[i][1] /= (float)nclass[i];
	    error_matrix[i][0] /= (float)(sg->nnodes - nclass[i]);
	    nlabels++;
	  }
	}

	for(i=1; i <= sg->nlabels; i++){
	  if (nclass[i]!=0)
	    error += (error_matrix[i][0]+error_matrix[i][1]);
	}

	Acc = 1.0-(error/(2.0*nlabels));

	for(i=0; i <= sg->nlabels; i++)
	  free(error_matrix[i]);
	free(error_matrix);
	free(nclass);

	return(Acc);
}

//	kNN classification 
void knn_kNNClassifying(Subgraph *sgtrain, Subgraph* sg, int k){
	int i, j, l, m;		// Iterators

	// Classifies every node 'i' in the test subgraph 'sg'
	for(i = 0; i < sg->nnodes; i++){
		//printf("classifying node %d (%.2f, %.2f)...\n", i, sg->node[i].feat[0], sg->node[i].feat[1]);

		float* distancesToNN = (float*)malloc(sizeof(float)*k); // Ordered list of the distances to the k-NN
		int* kNN = (int*)malloc(sizeof(int)*k); // Ordered list of the k-NN
		for (j = 0; j < k; j++) distancesToNN[j] = FLT_MAX; // Initializing list with infinity
		for (j = 0; j < k; j++) kNN[j] = NIL; // Initializing list with NIL

		// Finds the k nearest neighbours in sgtrain, comparing each node ('j')
		for (j = 0; j < sgtrain->nnodes; j++){
			// TODO: distinct distance functions
			float distance = knn_EuclDist(sg->node[i].feat,sgtrain->node[j].feat,sg->nfeats);
			//printf("neighbour [%d] is at distance %lf\n", j, distance);
			// tries to place it in the ordered list
			for (l = 0; l < k; l++){
				//printf("list[%d] is %lf\n", l, distancesToNN[l]);
				if (distance > distancesToNN[l]) {printf("is further, break\n"); break;} // Further than this neighbour: end
				else if (distance < distancesToNN[l]) { // Closer than this neighbour
					//printf("is closer than this neighbour, ");
					if (l == k-1){ // Closer than the closest neighbour: shift and end
						//printf("is closer than closest: shift and end\n");
						for (m = 0; m < l; m++) {distancesToNN[m] = distancesToNN[m+1]; kNN[m] = kNN[m+1];}
						distancesToNN[l] = distance; kNN[l] = j;
						break;
					}
					 else if (distance >= distancesToNN[l+1]){ // Not closer than the next neighbour: shift and continue
						//printf("is closer than next: shift and continue\n");
						for (m = 0; m < l; m++) {distancesToNN[m] = distancesToNN[m+1]; kNN[m] = kNN[m+1];}
						distancesToNN[l] = distance; kNN[l] = j;
						continue;
					} else {/*printf("is closer than the next, continue\n");*/ continue;} // Closer than the next neighbour: continue					
				}
				else continue; // Equal to this neighbour: continue
			}
		}

		/*printf("kNN are: ");
		for (j = 0; j < k; j++) printf(" %d,",kNN[j]);
		printf("\n");
		printf("distancesToNN are: ");
		for (j = 0; j < k; j++) printf(" %f,",distancesToNN[j]);
		printf("\n");*/

		// Check dominant hypothesis of kNN
		int* labelCount = (int*)calloc(sg->nlabels, sizeof(int));
		int dominantLabel = -1; int dominantValue = 0;
		for (j = 0; j < k; j++) labelCount[sgtrain->node[kNN[j]].truelabel-1] += 1;
		for (j = 0; j < sg->nlabels; j++) {
			if (labelCount[j] > dominantValue) {
				dominantValue = labelCount[j];
				dominantLabel = j+1;
			}
		}	 

		sg->node[i].label = dominantLabel;

		free(labelCount);
		free(distancesToNN);
		free(kNN);
	}
}

// Compute Euclidean distance between feature vectors
float knn_EuclDist(float *f1, float *f2, int n){
	int i;
	float dist=0.0f;

	for (i=0; i < n; i++){
		dist += (f1[i]-f2[i])*(f1[i]-f2[i]);
	}

	return(sqrt(dist));
}