#!/bin/bash
datasets=(cone-torus SpamBase2 mpeg7_BAS ntl skin Produce L3_4dist)
labels=(3 2 70 2 2 14 3)
kmax=(40 50 20 50 100 50 50)
c=(2048 512 2 128 2048 32 0.125)
g=(0.5 0.0001225 0.0078125 8 0.5 0.0078125 2)
splits=(5 5 5 5 5 5 5)

rm -rf resultsB
mkdir resultsB

for i in 6
do
	echo "Running test for set ${datasets[$i]}"

	mkdir resultsB/${datasets[$i]}
	mkdir resultsB/${datasets[$i]}/opf
	mkdir resultsB/${datasets[$i]}/svm
	mkdir resultsB/${datasets[$i]}/knn1
	mkdir resultsB/${datasets[$i]}/knn3
	mkdir resultsB/${datasets[$i]}/knn5
	cp -f data/${datasets[$i]}.opf ${datasets[$i]}.opf
	./run_OPFB_on_dataset.sh ${datasets[$i]} ${labels[$i]} ${kmax[$i]} ${splits[$i]}
	./run_knnB_on_dataset.sh ${datasets[$i]} ${labels[$i]} 1 ${splits[$i]}
	./run_knnB_on_dataset.sh ${datasets[$i]} ${labels[$i]} 3 ${splits[$i]}
	./run_knnB_on_dataset.sh ${datasets[$i]} ${labels[$i]} 5 ${splits[$i]}
	./run_SVMstudy_on_dataset.sh ${datasets[$i]} ${labels[$i]} ${c[$i]} ${g[$i]} ${splits[$i]}
	mv -f resultsB results_case_study

done
