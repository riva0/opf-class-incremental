# this script requires three arguments:
# $1: the dataset name (i.e. if running tests for 'dataset1.dat', the dataset name is 'dataset')
# $2: the number of classes in said dataset
# $3: k-neighbours amount
echo "Simulating experiment for $1, with $2 classes. k = $3"

incsize=$4
initsize=$((100/incsize))


# Runs experiments where all classes begin as S_0
for (( i=1; i<=7; i++ ))
do
	cp -f $1.opf $1.dat
	# Splits the dataset into S and T
	echo "	Splitting 'S' and 'T'..."
	{
		echo "./opf_split $1.dat 0.5 0 0.5 1"
		./opf_split $1.dat 0.5 0 0.5 1
		mv -f training.dat $1S.dat
		mv -f testing.dat $1T.dat
	} &>> /dev/null

	echo "	S_0 is of class $i"

	# Splits the S dataset into one file for each class
	echo "	Splitting classes"
	{
		echo "./opf_class_split $1S.dat 0"
		./opf_class_split $1S.dat 0
	} &>> /dev/null
	# Splits the initial class's file into two subsets; one is S_0, the other belongs to I
	echo "	Splitting initial class"
	{
		echo "./opf_split class$i.dat 0.5 0 0.5 0"
		./opf_split class$i.dat 0.5 0 0.5 0
	} &>> /dev/null
	mv -f training.dat $1S_0.dat
	mv -f testing.dat class$i.dat

	# build 1-class model on S_0 with kNN and test S_0 against T
	echo "	Initial training on S_0"
	{
		cp -f $1T.dat testS_0.dat
		echo "./knn_classify $1S_0.dat testS_0.dat $3"
		./knn_classify $1S_0.dat testS_0.dat $3
	} &>> /dev/null

	# Running experiment with uniform increments
	# Merging all incremental datasets into $1I
	echo "	Merging class files..."
	{
		echo "./opf_merge class*.dat"
		./opf_merge class*.dat
	} &>> /dev/null
	mv -f merged.dat $1I.dat

	# Splitting $1I into increments
	echo "	Splitting '$1I' into increments..."
	{
		echo "./opf_incremental_split $1I.dat 1 0 0.$(printf "%02d" $initsize) $incsize 0"
		./opf_incremental_split $1I.dat 1 0 0.$(printf "%02d" $initsize) $incsize 0
	} &>> /dev/null
	cp $1S_0.dat merged.dat
	# Iterates through all of i segments
	for (( j=0; j<$incsize; j++ ))
	do
		echo "	Currently inc_training on II_$(printf "%04d" $j)"
		{	
			echo "./opf_merge merged.dat $1II_$(printf "%04d" $j).dat"
			./opf_merge merged.dat $1II_$(printf "%04d" $j).dat
			cp merged.dat $1II_$(printf "%04d" $j).dat


			cp -f $1T.dat testII_$(printf "%04d" $j).dat
			echo "./knn_classify $1II_$(printf "%04d" $j).dat testII_$(printf "%04d" $j).dat $3"
			./knn_classify $1II_$(printf "%04d" $j).dat testII_$(printf "%04d" $j).dat $3

		} &>> /dev/null
	done

	# finally, testing full training on 'S' against 'T'
	echo "	Full training..."
	{
		cp -f $1T.dat testFull.dat
		echo "./knn_classify $1S.dat testFull.dat $3"
		./knn_classify $1S.dat testFull.dat $3

	} &>> /dev/null

	echo "Outputting acc to acc file..."
	{
		echo "Full Accuracy"
		cat testFull.dat.acc

		echo "Accuracy - I"
		cat testS_0.dat.acc
		for (( j=0; j<$incsize; j++ ))
		do
			cat testII_$(printf "%04d" $j).dat.acc
		done


	} &> resultsA/$1/knn$3/accK$i.txt


	echo "Outputting time to time file..."
	{
		echo "Full Time (seconds)"
		cat testFull.dat.time

		echo "Time - I"
		cat testS_0.dat.time
		for (( j=0; j<$incsize; j++ ))
		do
			cat testII_$(printf "%04d" $j).dat.time
		done
	} &> resultsA/$1/knn$3/timeK$i.txt

	rm -f *.dat* classifier.opf
done